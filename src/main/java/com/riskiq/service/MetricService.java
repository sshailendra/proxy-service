package com.riskiq.service;


import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.riskiq.domain.FileMetrics;
import com.riskiq.domain.ServiceOwner;
import com.riskiq.repository.ServiceOwnerRepository;
import com.riskiq.repository.UserRepository;

/**
 * Service to calculate latest file metrics from user and servcie owner repository.
 *
 * @author sshailedra
 * @created 12/31/2020
 */
@Service
public class MetricService
{
    private ServiceOwnerRepository serviceOwnerRepository;
    private UserRepository userRepository;
    public static Map<String, String> ownerByService= new HashMap<>();
	
    private static final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);

	private static final List<FileMetrics> fileMetrics= new ArrayList<>();

	private final Object lock = new Object();

    @Autowired
    public  MetricService(ServiceOwnerRepository serviceOwnerRepository, UserRepository userRepository) {
        this.serviceOwnerRepository = serviceOwnerRepository;
        this.userRepository = userRepository;
        
        final ScheduledFuture<?> futureTask = 
                scheduler.scheduleAtFixedRate(new FileTask(), 0, 300, TimeUnit.SECONDS);
    }
    
    public List<FileMetrics> getAllFileMetrics() throws IOException, InterruptedException, ExecutionException
    {
		if(fileMetrics.isEmpty())
		{
			synchronized(lock)
			{
				while(fileMetrics.isEmpty())
				{
					lock.wait();
				}
				lock.notifyAll();
			}
		}
        return fileMetrics;
    }

	public List<ServiceOwner> getAllServiceOwner()
	{
		List<ServiceOwner> serviceowners = new ArrayList<>();

		try (FileReader reader = new FileReader(serviceOwnerRepository.getPath().getFile());
			 BufferedReader br = new BufferedReader(reader)) {

			String line;

			while ((line = br.readLine()) != null) {
				String[] lineArray = line.split(",");
				ServiceOwner serviceowner = new ServiceOwner();
				serviceowner.setServiceName((lineArray[0]));
				serviceowner.setOwner(lineArray[1]);
				serviceowners.add(serviceowner);

				ownerByService.put(lineArray[0], lineArray[1]);
			}

		} catch (IOException e) {
			System.err.format("IOException: %s%n", e);
		}

		return serviceowners;
	}

	public String getOwnerByServiceName(String id)
	{
		if(ownerByService.isEmpty())
		{
			getAllServiceOwner();
		}
		
		return ownerByService.get(id);
	}

 private class FileTask implements Runnable
 {
	@Override
	public void run()
	{
		synchronized(lock)
		{
			fileMetrics.clear();

			FileMetrics userRepoMetric = new FileMetrics();

			userRepoMetric.setFile(userRepository.getPath());

			try
			{
				userRepoMetric.setNumerOfLines(userRepository.size());
			} catch (IOException e)
			{
				e.printStackTrace();
			}

			fileMetrics.add(userRepoMetric);

			FileMetrics serviceOwnerMetric = new FileMetrics();
			serviceOwnerMetric.setFile(serviceOwnerRepository.getPath());
			try
			{
				serviceOwnerMetric.setNumerOfLines(serviceOwnerRepository.size());
			} catch (IOException e)
			{
				e.printStackTrace();
			}

			fileMetrics.add(serviceOwnerMetric);
			lock.notifyAll();
		}
	}
 }
}
       
