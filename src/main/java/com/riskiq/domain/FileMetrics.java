package com.riskiq.domain;

import java.net.URL;

public class FileMetrics 
{
	private URL file;
	private long numerOfLines;

	public URL getFile() {
		return file;
	}
	public void setFile(URL file) {
		this.file = file;
	}
	public long getNumerOfLines() {
		return numerOfLines;
	}
	public void setNumerOfLines(long numerOfLines) {
		this.numerOfLines = numerOfLines;
	}
}
