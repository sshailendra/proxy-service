package com.riskiq.repository;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Stream;

import org.springframework.stereotype.Component;

import com.riskiq.domain.ServiceOwner;
import com.riskiq.domain.User;

@Component
public class ServiceOwnerRepository extends CrudRepository<ServiceOwner, URL>{

	public ServiceOwnerRepository(URL path) {
		super(path);
	}
	
	 @Override
	    public long size() throws IOException {

	        try {
	            Stream<String> lines = Files.lines(Paths.get(getPath().toURI()));
	            return lines.count();
	        }
	        catch (URISyntaxException ex) {
	            System.out.println(ex);
	            return 0;
	        }
	    }
}
