package com.riskiq.resource;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.ExecutionException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.riskiq.domain.FileMetrics;
import com.riskiq.domain.ServiceOwner;
import com.riskiq.service.MetricService;


/**
 * This class translates rest API to java method.
 *
 * @author sshailedra
 * @created 12/31/2020
 */
@RestController
public class MetricsResource {

  @Autowired
  private MetricService metricService;

  
  @RequestMapping(value = "api/metrics/", method = RequestMethod.GET)
  public List<FileMetrics> getAllMetrics() throws IOException, ExecutionException, InterruptedException {

	return metricService.getAllFileMetrics();
  }
  
  @RequestMapping(value = "/api/serviceowners", method = RequestMethod.GET)
  public List<ServiceOwner> getAllServiceOwners() throws IOException
  {
	return metricService.getAllServiceOwner();
  }
  
  @RequestMapping(value = "/api/serviceowners/{serviceName}", method = RequestMethod.GET)
  public String getOwnerByServiceName(@PathVariable String serviceName) throws IOException {
      return metricService.getOwnerByServiceName(serviceName);
  }
}
