package com.riskiq.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(value = "riskiq.proxy.serviceowner")
public class ServiceOwnerConfig {
	private String serviceownerFile;

	public String getServiceownerFile()
	{
		return serviceownerFile;
	}

	public void setServiceownerFile(String serviceownerFile)
	{
		this.serviceownerFile = serviceownerFile;
	}
	 
}









