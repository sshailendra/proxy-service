package com.riskiq;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.net.URL;
import java.util.Collections;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.riskiq.config.ProxyConfig;
import com.riskiq.config.ServiceOwnerConfig;
import com.riskiq.repository.ServiceOwnerRepository;
import com.riskiq.repository.UserRepository;
import com.riskiq.service.MetricService;

@RunWith(SpringRunner.class)
@WebMvcTest
public class ProxyApplicationTests {

	@Autowired
	MockMvc mockMVC;
	
	@MockBean
	ProxyConfig proxyConfig;
	
	@MockBean
	ServiceOwnerConfig serviceOwnerConfig;
	
	@MockBean
	MetricService metricService;
	
	@MockBean
	UserRepository userRepository;
	
	@MockBean
	ServiceOwnerRepository serviceOwnerRepository;
	
	@Test
	public void test_contextLoads_mertics() throws Exception
	{
		Mockito.when(metricService.getAllFileMetrics()).thenReturn(Collections.emptyList());
		
		MvcResult result = mockMVC.perform(MockMvcRequestBuilders.get("/api/metrics/")).andExpect(status().isOk()).andReturn();
		
		System.out.println(result.getResponse().getContentAsString());
	}
	

	@Test
	public void test_contextLoads_serviceowners() throws Exception
	{
		Mockito.when(metricService.getAllFileMetrics()).thenReturn(Collections.emptyList());
		
		MvcResult result = mockMVC.perform(MockMvcRequestBuilders.get("/api/serviceowners")).andExpect(status().isOk()).andReturn();
		
		System.out.println(result.getResponse().getContentAsString());
	}
	
	@Test
	public void test_contextLoads_serviceownersbyservicename() throws Exception
	{
		Mockito.when(metricService.getAllFileMetrics()).thenReturn(Collections.emptyList());
		
		MvcResult result = mockMVC.perform(MockMvcRequestBuilders.get("/api/serviceowners/1234")).andExpect(status().isOk()).andReturn();
		
		System.out.println(result.getResponse().getContentAsString());
	}

}
