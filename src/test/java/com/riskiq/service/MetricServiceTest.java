package com.riskiq.service;

import com.riskiq.domain.FileMetrics;
import com.riskiq.domain.ServiceOwner;
import com.riskiq.repository.ServiceOwnerRepository;
import com.riskiq.repository.UserRepository;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

import java.net.URL;
import java.util.List;

/**
 * Test for {@link MetricService}
 * @author sshailendra
 * @created 12/31/2020
 */
public class MetricServiceTest
{
    @Test
    public void testGetAllFileMetrics() throws Exception
    {
        ServiceOwnerRepository mockServiceOwnerRepository = Mockito.mock(ServiceOwnerRepository.class);
        UserRepository mockUserRepository = Mockito.mock(UserRepository.class);
        MetricService service = new MetricService(mockServiceOwnerRepository, mockUserRepository);
        List<FileMetrics> metrics = service.getAllFileMetrics();

        Assert.assertEquals(metrics.size(), 2);
    }
}
