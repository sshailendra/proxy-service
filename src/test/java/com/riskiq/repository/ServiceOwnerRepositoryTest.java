package com.riskiq.repository;

import com.google.common.io.Resources;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Test for {@link ServiceOwnerRepository}
 * @author sshailendra
 * @created 12/31/20
 */
public class ServiceOwnerRepositoryTest
{
    @Test
    public void testGetSize() throws Exception
    {
        ServiceOwnerRepository repository = new ServiceOwnerRepository(Resources.getResource("testserviceownerGetSize.txt"));
        long size = repository.size();
        assertEquals(size, 4);
    }
}
